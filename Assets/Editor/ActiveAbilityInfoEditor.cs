﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using UnityEditor.AnimatedValues;

//[CustomPropertyDrawer(typeof(ActiveAbilityInfo))]
public class ActiveAbilityEditor : PropertyDrawer
{
    private ActiveAbilityInfo instance;

    //private SerializedProperty requiresTarget;
    //private SerializedProperty targetLayers;
    //private SerializedProperty channeling;
    //private SerializedProperty impactWhileChanneling;
    //private SerializedProperty maxChannelingTime;

    private AnimBool targetProperties;
    private AnimBool channelingProperties;

    private void OnEnable ()
    {
        targetProperties = new AnimBool(false);
        channelingProperties = new AnimBool(false);
    }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        var requiresTarget = property.FindPropertyRelative("requiresTarget");
        var targetLayers = property.FindPropertyRelative("targetLayers");
        var channeling = property.FindPropertyRelative("channeling");
        var impactWhileChanneling = property.FindPropertyRelative("impactWhileChanneling");
        var maxChannelingTime = property.FindPropertyRelative("maxChannelingTime");

        EditorGUI.PropertyField(new Rect(position.x, position.y, position.width, 25f), requiresTarget);
        targetProperties.target = requiresTarget.boolValue;
    }
}
