﻿using System;
using UnityEngine;

[Serializable]
public struct NoiseInfo
{
    public float amount;
    public Vector3 sourcePosition;

    public float NoiseAtPosition (Vector3 position)
    {
        var distance = Vector3.Distance(sourcePosition, position);
        var noiseFactor = GameSettings.Instance.enemiesHearingCurve.Evaluate(distance);

        return amount * noiseFactor;
    }
}
