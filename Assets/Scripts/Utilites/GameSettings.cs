﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(fileName = "Game settings", menuName = "Game settings")]
public class GameSettings : ScriptableObject
{
    public AnimationCurve enemiesHearingCurve;

    public static GameSettings Instance
    {
        get
        {
            if (!instance) {
                instance = Resources.Load<GameSettings>("Game settings");
            }

            return instance;
        }
    }

    private static GameSettings instance;

}
