﻿using UnityEngine;
using System.Collections;

public class Collectable : ObjectProperty
{
    public Sprite icon;

    [TextArea]
    public string description;

    public bool hasDurability;
    public int currentDurability;
    public int totalDurability;

    public Rarity rarity;
    public ItemType type;

    public float weight;
    public bool stackable;
    public int stackSize;
    public string slug;

    public override void OnClick ()
    {
        Inventory.Collect(this);
        Destroy(gameObject);
    }
}
