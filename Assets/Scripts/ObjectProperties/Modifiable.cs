﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class Modifiable : MonoBehaviour
{
    private HashSet<PassiveAbility> modifiers = new HashSet<PassiveAbility>();

    public void AddModifier (PassiveAbility modifier)
    {
        modifiers.Add(modifier);
		modifier.Init(gameObject);
    }

    private void ApplyModifiers ()
    {
        foreach (var modifier in modifiers.Where(modifier => modifier.basicInfo.isActive)) {
            modifier.Apply(gameObject);
        }
    }

	void Update ()
	{
	    ApplyModifiers();
	}
}
