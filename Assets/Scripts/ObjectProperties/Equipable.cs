﻿using UnityEngine;
using System.Collections;

public enum EquipmentSlot
{
    Hand, Body
}

public class Equipable : MonoBehaviour
{
    public EquipmentSlot slot;
}
