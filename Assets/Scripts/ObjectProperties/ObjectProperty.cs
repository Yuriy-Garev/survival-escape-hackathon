﻿using UnityEngine;
using System.Collections;

public abstract class ObjectProperty : MonoBehaviour
{
    public abstract void OnClick();
}
