﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class InventoryMenuItem : MonoBehaviour
{
    public GameObject tooltipPrefab;
    public Text titleText;
    public Text durabilityText;
    public Text weightText;
    public Image iconSpot;
    public Outline equipmentOutline;
    
    public InventoryItem Item
    {
        get { return item; }
        set
        {
            item = value;
            SetProperties();
        }
    }

    private InventoryItem item;
    private bool isHovering;

    public void ShowTooltip ()
    {

    }

    public void OnPointerEnter ()
    {
        isHovering = true;
    }
    
    public void OnPointerExit ()
    {
        isHovering = false;
    }

    public void OnPointerClick (BaseEventData data)
    {
        var eventData = (PointerEventData)data;
        switch (eventData.button) {
            case PointerEventData.InputButton.Right:
                if (item.Favorite) {
                    item.Favorite = false;
                    Inventory.UpdateWindow();
                } else {
                    Inventory.Throw(item);
                }
                break;
            case PointerEventData.InputButton.Left:
                if (!item.Equipable) {
                    break;
                }

                if (!item.IsEquiped) {
                    Equipment.Equip(item);
                } else {
                    Equipment.Unequip(item);
                }

                equipmentOutline.enabled = item.IsEquiped;
                break;
        }
    }

    public void UpdateMenuItem ()
    {
        SetProperties();
    }

    private void Update ()
    {
        if (!isHovering) {
            return;
        }

        for (int i = 0; i < 8; i++) {
            var key = (int) KeyCode.Alpha1 + i;
            if (Input.GetKeyDown((KeyCode)key)) {
                Item.Favorite = true;
                Item.HotKey = (KeyCode) key;
            }
        }
    }

    private void SetProperties ()
    {
        titleText.text = string.Format("{0} ({1})", item.Title, item.Weight);
        //iconSpot.sprite = item.Icon;
        durabilityText.text = string.Format("{0}%", (int)((float)item.CurrentDurability / item.TotalDurability * 100));
        weightText.text = item.Weight.ToString("0.00");
        equipmentOutline.enabled = item.IsEquiped;
    }


}
