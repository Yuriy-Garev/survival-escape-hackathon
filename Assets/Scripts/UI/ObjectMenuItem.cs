﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ObjectMenuItem : MonoBehaviour
{
    public ObjectProperty Property
    {
        get { return property; }
        set
        {
            property = value;
            propertyText.text = property.GetType().Name;
        }
    }

    private ObjectProperty property;
    private Text propertyText;

    private void Awake ()
    {
        propertyText = GetComponentInChildren<Text>();
    }

    public void OnClick ()
    {
        Property.OnClick();
    }
}
