﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class InventoryWindow : MonoBehaviour
{
    public RectTransform itemsBase;
    public GameObject window;
    public GameObject inventoryItemPrefab;

    private List<InventoryMenuItem> currentItems = new List<InventoryMenuItem>();
    private Predicate<InventoryItem> currentFilter; 

    private const string PrefsKey = "Tab name";
    private const string InventoryButton = "Inventory";

    public void Show ()
    {
        ActivateWindow(true);
        ClearWindow();
        CreateItems();
        ShowFavorites();
    }

    public void UpdateWindow ()
    {
        var inventoryItems = Inventory.GetItems();
        foreach (var item in currentItems.Where(item => !inventoryItems.Contains(item.Item))) {
            Destroy(item.gameObject);
        }
        currentItems.RemoveAll(item => item == null);

        foreach (var menuItem in currentItems) {
            menuItem.UpdateMenuItem();
        }

        Filter(currentFilter);
    }

    public void ShowFavorites ()
    {
        Filter(item => item.Favorite);
    }

    public void ShowIngridients ()
    {
        Show(ItemType.Ingridients);
    }

    public void ShowTools ()
    {
        Show(ItemType.Tools);
    }

    public void ShowWhatever ()
    {
        Show(ItemType.Whatever);
    }

    public void OverweightWarning ()
    {

    }

    private void Show (ItemType tab)
    {
        Filter(tab);
    }

    private void Filter(ItemType tab)
    {
        Filter(item => item.Type == tab);
    }

    private void Filter (Predicate<InventoryItem> predicate)
    {
        currentFilter = predicate;
        foreach (var item in currentItems) {
            item.gameObject.SetActive(predicate.Invoke(item.Item));
        }
    }


    private void CreateItems ()
    {
        var items = Inventory.GetItems();
        foreach (var item in items) {
            var menuItem = Instantiate(inventoryItemPrefab);
            menuItem.transform.SetParent(itemsBase);
            menuItem.SetActive(false);

            var itemScript = menuItem.GetComponent<InventoryMenuItem>();
            itemScript.Item = item;
            currentItems.Add(itemScript);
        }
    }

    public void ClearWindow ()
    {
        currentItems.Clear();
        foreach (var result in itemsBase.Cast<Transform>()) {
            Destroy(result.gameObject);
        }
    }

    private void ActivateWindow (bool activate)
    {
        window.SetActive(activate);
    }

    private void Update ()
    {
        if (Input.GetButtonDown(InventoryButton)) {
            if (!window.activeInHierarchy) {
                Show();
            } else {
                ActivateWindow(false);
            }
        }

    }
}
