﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class AbilitiesMenu : MonoBehaviour
{
    public RectTransform itemsBase;
    public GameObject itemPrefab;

	private List<AbilityItem> items = new List<AbilityItem>();

    public void UpdateMenu ()
    {
        ClearPanel();
        RefreshAbilities();
    }

    private void ClearPanel ()
    {
        foreach (var child in itemsBase.Cast<Transform>()) {
            Destroy(child.gameObject);
        }

        items.Clear();
    }

    private void RefreshAbilities ()
    {
        var newAbilities = AbilitiesManager.GetAvailableAbilities<ActiveAbility>();
        foreach (var ability in newAbilities) {
            var item = Instantiate(itemPrefab);
            item.transform.SetParent(itemsBase, false);

            var abilityItem = item.GetComponent<AbilityItem>();
            abilityItem.Ability = ability;
            items.Add(abilityItem);
        }
    }
}
