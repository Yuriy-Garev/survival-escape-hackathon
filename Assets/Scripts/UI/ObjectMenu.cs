﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ObjectMenu : MonoBehaviour
{
    public RectTransform itemsBase;
    public GameObject itemPrefab;

    private Vector2 menuPosition;

    public void Show (GameObject target)
    {
        ClearMenu();

        itemsBase.gameObject.SetActive(true);
        menuPosition = Input.mousePosition;
        itemsBase.position = menuPosition;

        foreach (var property in target.GetComponents<ObjectProperty>()) {
            CreateProperty(property);
        }
    }

    private void Update ()
    {
        itemsBase.position = menuPosition;

        if (Input.GetKeyDown(KeyCode.Escape)) {
            HideMenu();
        }

        if (!Input.GetMouseButtonDown(0) || !itemsBase.gameObject.activeInHierarchy) {
            return;
        }

        var eventSystem = EventSystem.current;
        var eventData = new PointerEventData(eventSystem) {position = Input.mousePosition};

        List<RaycastResult> results = new List<RaycastResult>();
        eventSystem.RaycastAll(eventData, results);

        if (results.All(res => res.gameObject != itemsBase.gameObject)) {
            HideMenu();
        }
    }

    private void HideMenu ()
    {
        itemsBase.gameObject.SetActive(false);
    }

    private void ClearMenu()
    {
        for (int index = 0; index < itemsBase.childCount; index++) {
            Destroy(itemsBase.GetChild(index).gameObject);
        }
    }

    private void CreateProperty (ObjectProperty property)
    {
        var newItem = Instantiate(itemPrefab);
        newItem.transform.SetParent(itemsBase);

        var item = newItem.GetComponent<ObjectMenuItem>();
        item.Property = property;

        var button = newItem.GetComponent<Button>();
        button.onClick.AddListener(() => itemsBase.gameObject.SetActive(false));
    }
}
