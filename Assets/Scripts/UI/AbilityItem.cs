﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AbilityItem : MonoBehaviour
{
    public ActiveAbility Ability
    {
        get { return ability; }
        set
        {
            ability = value;
            mainImage.sprite = ability.basicInfo.icon;
        }
    }

    private Outline outline;
    private Image mainImage;
    private ActiveAbility ability;

    public void ChooseAbility ()
    {
        AbilitiesManager.CurrentAbility = Ability;
    }

    private void Awake ()
    {
        outline = GetComponent<Outline>();
        mainImage = GetComponent<Image>();
    }

    private void Update ()
    {
        outline.enabled = ability == AbilitiesManager.CurrentAbility;
    }
}
