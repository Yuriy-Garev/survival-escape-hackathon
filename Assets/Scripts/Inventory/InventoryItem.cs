﻿using System;
using UnityEngine;
using System.Collections;

public enum Rarity
{
    Regular, Uncommon, Rare, Epic, Legendary
}

public enum ItemType
{
    Ingridients, Tools, Whatever
}

public class InventoryItem : IEquatable<InventoryItem>
{
    public string Title { get; set; }
    public string Description { get; set; }

    public bool HasDurability { get; set; }
    public int TotalDurability { get; set; }
    public int CurrentDurability { get; set; }

    public ItemType Type { get; set; }

    public float Weight { get; set; }

    public bool Stackable { get; set; }

    public bool Favorite { get; set; }
    public KeyCode HotKey { get; set; }

    public bool Equipable { get; set; }
    public EquipmentSlot EquipmentSlot { get; set; }
    public bool IsEquiped { get; set; }

    public string Slug { get; set; }
    //public Sprite Icon { get; set; }

    public bool Equals(InventoryItem other)
    {
        if (ReferenceEquals(null, other)) { return false; }
        if (ReferenceEquals(this, other)) { return true; }
        return string.Equals(Title, other.Title) && string.Equals(Description, other.Description) && HasDurability == other.HasDurability && TotalDurability == other.TotalDurability && CurrentDurability == other.CurrentDurability && Type == other.Type && Weight == other.Weight && Stackable == other.Stackable && string.Equals(Slug, other.Slug);
    }

    public override bool Equals(object obj)
    {
        if (ReferenceEquals(null, obj)) { return false; }
        if (ReferenceEquals(this, obj)) { return true; }
        if (obj.GetType() != this.GetType()) { return false; }
        return Equals((InventoryItem) obj);
    }

    public override int GetHashCode()
    {
        unchecked {
            var hashCode = (Title != null ? Title.GetHashCode() : 0);
            hashCode = (hashCode * 397) ^ (Description != null ? Description.GetHashCode() : 0);
            hashCode = (hashCode * 397) ^ HasDurability.GetHashCode();
            hashCode = (hashCode * 397) ^ TotalDurability;
            hashCode = (hashCode * 397) ^ CurrentDurability;
            hashCode = (hashCode * 397) ^ (int) Type;
            hashCode = (hashCode * 397) ^ Weight.GetHashCode();
            hashCode = (hashCode * 397) ^ Stackable.GetHashCode();
            hashCode = (hashCode * 397) ^ (Slug != null ? Slug.GetHashCode() : 0);
            return hashCode;
        }
    }
}