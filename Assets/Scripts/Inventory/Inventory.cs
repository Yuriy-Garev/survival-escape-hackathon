﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using LitJson;

public class Inventory : MonoBehaviour
{
    public float maximumWeight;
    public InventoryWindow window;

    private static Inventory instance;

    private List<InventoryItem> items;
    private List<GameObject> collectables;
    private float currentWeight;

    private const string FileName = "Items.json";
    private const string CollectablesPath = "Prefabs/Collectables";

    public static void Collect (Collectable item)
    {
        var newItem = ToInventoryItem(item);
        if (instance.currentWeight + newItem.Weight <= instance.maximumWeight) {
            instance.items.Insert(0, newItem);
            UpdateWindow();
        } else {
            instance.window.OverweightWarning();
        }
    }

    public static GameObject Throw (InventoryItem item)
    {
        if (item.IsEquiped) {
            Equipment.Unequip(item);
        }

        instance.items.Remove(item);
        UpdateWindow();

        var newItem = InstantiateItem(item);
        var rigidbody = newItem.GetComponent<Rigidbody>();
        if (rigidbody != null) {
            rigidbody.useGravity = true;
        }

        foreach (var collider in newItem.GetComponentsInChildren<Collider>()) {
            collider.enabled = true;
        }

        return newItem;
    }

    public static GameObject InstantiateItem (InventoryItem item)
    {
        var itemInstance =
            instance.collectables
            .Where(collectable => collectable.GetComponent<Collectable>() != null)
            .FirstOrDefault(collectable => collectable.GetComponent<Collectable>().slug.Equals(item.Slug));

        if (itemInstance == null) {
            return null;
        }

        return Instantiate(itemInstance, instance.transform.position, Quaternion.identity) as GameObject;
    }

    public static void UpdateWindow ()
    {
        instance.window.UpdateWindow();
    }

    public static List<InventoryItem> GetItems ()
    {
        return new List<InventoryItem>(instance.items);
    } 

    private static InventoryItem ToInventoryItem (Collectable collectable)
    {
        var equipable = collectable.GetComponent<Equipable>();
        var newItem = new InventoryItem {
            Title = collectable.gameObject.name,
            Description = collectable.description,
            HasDurability = collectable.hasDurability,
            CurrentDurability = collectable.currentDurability,
            TotalDurability = collectable.totalDurability,
            Slug = collectable.slug,
            Stackable = collectable.stackable,
            Type = collectable.type,
            Weight = collectable.weight,
            Equipable = equipable != null,
        };

        if (equipable != null) {
            newItem.EquipmentSlot = equipable.slot;
        }

        return newItem;
    }

    private void Awake ()
    {
        if (instance != null) {
            Destroy(this);
            return;
        }

        instance = this;
    }

    private void Start ()
    {
        LoadItems();
        LoadCollectables();

        currentWeight = items.Sum(item => item.Weight);
    }

    private void OnDestroy ()
    {
        //Serialize ();
        instance = null;
    }

    private void LoadItems()
    {
        items = new List<InventoryItem>();
        var filePath = Application.dataPath + "/" + FileName;
        if (!File.Exists(filePath)) {
            return;
        }

        var fileReader = File.OpenText(filePath);
        var jsonString = fileReader.ReadToEnd();

		JsonMapper.RegisterImporter<string, float> (input => float.Parse (input));
        items = JsonMapper.ToObject<List<InventoryItem>>(jsonString);
    }

    private void LoadCollectables ()
    {
        collectables = Resources.LoadAll<GameObject>(CollectablesPath).ToList();
    }

    private void Serialize ()
    {
        JsonMapper.RegisterExporter<float> ((num, writer) => writer.Write (num.ToString()));
			
        var filePath = Application.dataPath + "/" + FileName;
        using (var writer = new StreamWriter(File.OpenWrite(filePath))) {
            var json = JsonMapper.ToJson(items);
            writer.Write(json);
        }
    }
}
