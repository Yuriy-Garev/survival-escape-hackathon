﻿using UnityEngine;
using System.Collections;

public class EnemyThinker : Thinker
{
    public void Distract (NoiseInfo noise)
    {

        var oldNoise = Remember<NoiseInfo>(AIConstants.NoiseInfoKey);
        var position = transform.position;
        if (!(oldNoise.NoiseAtPosition(position) < noise.NoiseAtPosition(position))) {
            return;
        }

        Remember(AIConstants.NoiseInfoKey, noise);
        Remember(AIConstants.IsDistractedKey, true);
    }

    protected override void Start()
    {
        base.Start();

        Distraction.OnDistract += Distract;
        PlayerNoise.OnMovementNoise += Distract;
    }

    protected override void OnDestroy()
    {
        base.OnDestroy();

        Distraction.OnDistract -= Distract;
        PlayerNoise.OnMovementNoise += Distract;
    }
}
