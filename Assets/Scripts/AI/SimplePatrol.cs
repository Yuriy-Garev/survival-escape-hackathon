﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(fileName = "Simple patrol", menuName = "Brain/Simple patrol")]
public class SimplePatrol : EnemyBrain
{
    [MinMaxRange(1f, 25f)]
    public RangedFloat patrolingDistance;

    public float detectingRange;
    public float forwardDetectionRange;
    public float forwardDetectionAngle;

    public LayerMask detectionLayerMask;

    private Ray forwardRay;
    private Ray leftRay;
    private Ray rightRay;

    private Vector3 startingPoint;
    private Transform player;


    public override void Think(Thinker thinker)
    {
        base.Think(thinker);

        FindTargetToMove(thinker);
        UpdateDirections(thinker);
        DetectPlayer(thinker);
    }

    public override void Init (Thinker thinker)
    {
        startingPoint = thinker.transform.position;
    }

    public override void DrawGizmos(Thinker thinker)
    {
        Gizmos.DrawWireSphere(startingPoint, patrolingDistance.minValue);
        Gizmos.DrawWireSphere(startingPoint, patrolingDistance.maxValue);

        Gizmos.color = Color.red;
        Gizmos.DrawLine(thinker.transform.position, thinker.Remember<Vector3>(AIConstants.CurrentTargetKey));

        Gizmos.color = Color.cyan;
        Gizmos.DrawWireSphere(thinker.transform.position, detectingRange);

        Gizmos.DrawLine(thinker.transform.position, forwardRay.GetPoint(forwardDetectionRange));
        Gizmos.DrawLine(thinker.transform.position, leftRay.GetPoint(forwardDetectionRange));
        Gizmos.DrawLine(thinker.transform.position, rightRay.GetPoint(forwardDetectionRange));
    }

    private void FindTargetToMove (Thinker thinker)
    {
        var movement = thinker.GetComponent<EnemyMovement>();
        if (!movement) {
            return;
        }

        var playerDetected = thinker.Remember<bool>(AIConstants.IsPlayerDetectedKey);
        if (playerDetected) {
            movement.SetTarget(thinker.Remember<Vector3>(AIConstants.CurrentTargetKey));
            return;
        }

        var currentTarget = thinker.Remember<Vector3>(AIConstants.CurrentTargetKey);
        var nav = thinker.GetComponent<NavMeshAgent>();
        if (!nav) {
            movement.SetTarget(currentTarget);
            return;
        }

        if (nav.remainingDistance <= nav.stoppingDistance) {
            if (!isDistracted) {
                thinker.Remember(AIConstants.CurrentTargetKey, GetNewPatrolPoint());
            } else {
                if (!hasReachedDistractionPoint) {
                    thinker.Remember(AIConstants.StartDistractionTimeKey, Time.time);
                    hasReachedDistractionPoint = true;
                }

                var stayedEnough = Time.time - thinker.Remember<float>(AIConstants.StartDistractionTimeKey) >=
                                   distractionTime;
                isDistracted = !stayedEnough;
                if (stayedEnough) {
                    thinker.Remember(AIConstants.NoiseInfoKey, new NoiseInfo());
                }

                hasReachedDistractionPoint = isDistracted;
                var movementTarget = stayedEnough
                    ? GetNewPatrolPoint()
                    : thinker.Remember<Vector3>(AIConstants.CurrentTargetKey);
                thinker.Remember(AIConstants.CurrentTargetKey, movementTarget);
            }
        }

        movement.SetTarget(thinker.Remember<Vector3>(AIConstants.CurrentTargetKey));
    }

    private Vector3 GetNewPatrolPoint ()
    {
        bool found;
        NavMeshHit hit;

        do {
            var unitSphere = Random.onUnitSphere;
            var randomDirection = Vector3.ProjectOnPlane(unitSphere, Vector3.up).normalized;
            var minPoint = startingPoint + randomDirection * patrolingDistance.minValue;

            var directionToMax = (minPoint - startingPoint).normalized;
            var randomDistance = Random.Range(0, patrolingDistance.maxValue - patrolingDistance.minValue);
            var referencePoint = minPoint + directionToMax * randomDistance;

            found = NavMesh.SamplePosition(referencePoint, out hit, Mathf.Infinity, NavMesh.AllAreas);
        } while (!found);

        return hit.position;
    }

    private void UpdateDirections (Thinker thinker)
    {
        var currentTransform = thinker.transform;

        var forward = currentTransform.forward;
        var left = Quaternion.AngleAxis(-forwardDetectionAngle / 2, currentTransform.up) * forward;
        var right = Quaternion.AngleAxis(forwardDetectionAngle / 2, currentTransform.up) * forward;

        forwardRay = new Ray(currentTransform.position, forward);
        rightRay = new Ray(currentTransform.position, right);
        leftRay = new Ray(currentTransform.position, left);
    }

    private void DetectPlayer (Thinker thinker)
    {
        if (thinker.Remember<bool>(AIConstants.IsPlayerDetectedKey)) {
            thinker.Remember(AIConstants.CurrentTargetKey, player.position);
            return;
        }

        var colliders = Physics.OverlapSphere(thinker.transform.position, detectingRange, detectionLayerMask);
        var playerDetected = colliders.Length > 0;

        if (playerDetected) {
            PlayerDetected(thinker, colliders[0].transform);
            return;
        }

        RaycastHit hit;
        playerDetected = Physics.Raycast(forwardRay, out hit, forwardDetectionRange, detectionLayerMask);

        if (playerDetected) {
            PlayerDetected(thinker, hit.collider.transform);
            return;
        }

        playerDetected = Physics.Raycast(leftRay, out hit, forwardDetectionRange, detectionLayerMask);

        if (playerDetected) {
            PlayerDetected(thinker, hit.collider.transform);
            return;
        }

        playerDetected = Physics.Raycast(rightRay, out hit, forwardDetectionRange, detectionLayerMask);

        if (playerDetected) {
            PlayerDetected(thinker, hit.collider.transform);
        }
    }

    private void PlayerDetected (Thinker thinker, Transform player)
    {
        thinker.Remember(AIConstants.IsPlayerDetectedKey, true);
        thinker.Remember(AIConstants.CurrentTargetKey, player.position);

        isDistracted = false;
        hasReachedDistractionPoint = false;

        this.player = player;
    }
}
