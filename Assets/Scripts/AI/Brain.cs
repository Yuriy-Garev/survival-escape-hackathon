﻿using UnityEngine;
using System.Collections;

public abstract class Brain : ScriptableObject
{
    public abstract void Think(Thinker thinker);
    public virtual void Init(Thinker thinker) {}
    public virtual void DrawGizmos(Thinker thinker) {}
}
