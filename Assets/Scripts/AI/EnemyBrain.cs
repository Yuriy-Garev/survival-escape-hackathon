﻿using UnityEngine;
using System.Collections;

public abstract class EnemyBrain : Brain
{
    public float hearingThreashold;
    public float distractionTime;

    protected bool isDistracted;
    protected bool hasReachedDistractionPoint;

    public override void Think(Thinker thinker)
    {
        Distract(thinker);
    }

    protected virtual void Distract (Thinker thinker)
    {
        if (!thinker.Remember<bool>(AIConstants.IsDistractedKey)/* || isDistracted*/) {
            return;
        }

        var noise = thinker.Remember<NoiseInfo>(AIConstants.NoiseInfoKey);
        if (!IsDistracted(noise, thinker.transform.position)) {
            thinker.Remember(AIConstants.IsDistractedKey, false);
            return;
        }

        if (thinker.Remember<bool>(AIConstants.IsPlayerDetectedKey)) {
            return;
        }

        Debug.LogFormat("Distracted by noise. Value: {0}, position: {1}", noise.NoiseAtPosition(thinker.transform.position), noise.sourcePosition);
        isDistracted = true;
        hasReachedDistractionPoint = false;
        thinker.Remember(AIConstants.CurrentTargetKey, noise.sourcePosition);
        thinker.Remember(AIConstants.IsDistractedKey, false);
    }

    protected bool IsDistracted (NoiseInfo noise, Vector3 position)
    {
        return noise.NoiseAtPosition(position) >= hearingThreashold;
    }
}
