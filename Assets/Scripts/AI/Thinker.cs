﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class Thinker : MonoBehaviour
{
    public Brain brain;

    protected Dictionary<string, object> memory = new Dictionary<string, object>();

    public T Remember<T> (string key)
    {
        if (!memory.ContainsKey(key)) {
            return default(T);
        }

        return (T) memory[key];
    }

    public void Remember<T> (string key, T value)
    {
        memory[key] = value;
    }

    protected virtual void Start ()
    {
        brain.Init(this);
    }

    protected virtual void Update ()
    {
        brain.Think(this);
    }

    private void OnDrawGizmos ()
    {
        if (brain)
            brain.DrawGizmos(this);
    }

    protected virtual void OnDestroy ()
    {

    }
}

public class AIConstants
{
    public const string CurrentTargetKey = "currentTarget";
    public const string IsDistractedKey = "isDistracted";
    public const string StartDistractionTimeKey = "startDistractionKey";
    public const string NoiseInfoKey = "noiseInfo";
    public const string IsPlayerDetectedKey = "isPlayerDetected";

}
