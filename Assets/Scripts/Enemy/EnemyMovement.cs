﻿using UnityEngine;
using System.Collections;

public class EnemyMovement : MonoBehaviour
{
    public Transform[] patrolingPoints;
    public float detectingRange;
    public float forwardDetectionRange;
    public float forwardDetectionAngle;

    private NavMeshAgent nav;
    private Vector3 currentTarget;

    public void SetTarget (Vector3 target)
    {
        currentTarget = target;
    }

    public bool PlayerDetected { get; set; }

    void Awake ()
    {
        nav = GetComponent<NavMeshAgent>();
    }

    void Update ()
    {
        Move();
    }

    private void Move ()
    {
        nav.destination = currentTarget;
    }
}
