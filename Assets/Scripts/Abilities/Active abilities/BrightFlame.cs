﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(fileName = "Bright flame", menuName = "Active ability/Bright flame")]
public class BrightFlame : ActiveAbility
{
    public PassiveAbility lighterModifier;

    private GameObject player;

    private GameObject Player
    {
        get
        {
            if (player == null) {
                player = GameObject.FindGameObjectWithTag("Player");
            }

            return player;
        }
    }

    public override void Activate()
    {
        base.Activate();

        lighterModifier.basicInfo.isActive = true;

        var modifiable = Player.GetComponent<Modifiable>();
        modifiable.AddModifier(lighterModifier);
    }
}
