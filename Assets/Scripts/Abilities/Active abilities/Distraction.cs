﻿using UnityEngine;
using System.Collections;
using System;

[CreateAssetMenu(fileName = "Distraction", menuName = "Active ability/Distraction")]
public class Distraction : ActiveAbility
{
    public ParticleSystem particles;
    public float noiseAmount;

    private Vector3 position;

    public delegate void NoiseDelegate(NoiseInfo info);
    public static event NoiseDelegate OnDistract;

    public override void Activate(RaycastHit targetSpot)
    {
        position = targetSpot.point;

        SetupParticles();
        DistractEnemies();
    }

    private void DistractEnemies ()
    {
        var noise = new NoiseInfo {
            amount = noiseAmount,
            sourcePosition = position,
        };

        if (OnDistract != null) {
            OnDistract(noise);
        }    
    }

    private void SetupParticles()
    {
        var instance = Instantiate(particles, position, particles.transform.rotation) as ParticleSystem;
        instance.Play();

        var duration = instance.duration;
        Destroy(instance.gameObject, duration);
    }
}
