﻿using UnityEngine;
using System.Collections;
using System.Linq;

[CreateAssetMenu(menuName = "Active ability/Searing flame")]
public class SearingFlame : ActiveAbility
{
    public float baseRadius;
    public float radiusIncreaseFactor;
    public LayerMask layers;

    public Light lightEffect;
    public PassiveAbility modifier;

    private Transform player;
    private Light light;
    //private ParticleSystem particlesInstance;

    public override void Activate(float channelingTime)
    {
        FindPlayer();
        Flash(baseRadius + channelingTime * radiusIncreaseFactor);
    }

    public override void Activate()
    {
        FindPlayer();
        Flash(baseRadius);
    }

    private void FindPlayer ()
    {
        var playerInstance = GameObject.FindGameObjectWithTag("Player");
        player = playerInstance.transform;
    }

    private void Flash (float radius)
    {
        if (light == null) {
            light = Instantiate(lightEffect, player.position, lightEffect.transform.rotation) as Light;
            light.transform.SetParent(player);
        }

        AbilitiesManager.RunCoroutine(Grow(radius));
        Stun(radius);
    }

    private IEnumerator Grow (float radius)
    {
        while (Mathf.Abs(radius - light.range) > 0.1f) {
            light.range = Mathf.Lerp(light.range, radius, Time.deltaTime * 5f);
            yield return null;
        }

        Destroy(light.gameObject);
    }

    private void Stun (float radius)
    {
        var colliders = Physics.OverlapSphere(player.position, radius, layers);
        foreach (var result in colliders
            .Select(collider => collider.GetComponent<Modifiable>())
            .Where(modifiable => modifiable != null)) {
            result.AddModifier(modifier);
        }
    }
}
