﻿using System;
using UnityEngine;
using System.Collections;

public abstract class Ability : ScriptableObject
{
    public BasicAbilityInfo basicInfo;
}

[Serializable]
public class BasicAbilityInfo
{
    public string title;
    public string description;
    public Sprite icon;

    public bool isActive;
}
