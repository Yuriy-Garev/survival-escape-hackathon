﻿using UnityEngine;
using System.Collections;

public abstract class PassiveAbility : Ability
{
    public abstract void Apply(GameObject target);
	public virtual void Init(GameObject target) {}
}
