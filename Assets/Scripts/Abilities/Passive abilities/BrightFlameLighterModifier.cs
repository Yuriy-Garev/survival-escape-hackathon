﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(menuName = "Passive ability/Modifiers/Brite flame lighter modifier", fileName = "Brite flame lighter modifier")]
public class BrightFlameLighterModifier : PassiveAbility
{
    public float lighterRadiusIncrease;
    public float noizeIncrease;

    private GameObject target;
    private Lighter lighter;
    private Light light;

    private float referenceRadius;
    private bool previouslyEnabled;
    private bool isWorking;

    private Lighter Lighter
    {
        get
        {
            if (!lighter) {
                lighter = target.GetComponent<Lighter>();
            }

            return lighter;
        }
    }

    private Light Light
    {
        get
        {
            if (!light) {
                light = Lighter.lighter;
            }

            return light;
        }
    }

	public override void Init(GameObject target)
    {
		this.target = target;
        referenceRadius = Light.range;
        previouslyEnabled = Light.enabled;
        isWorking = false;
    }

    public override void Apply(GameObject target)
    {
        if (!previouslyEnabled && Light.enabled) {
            isWorking = true;
        }

        if (previouslyEnabled && !Light.enabled) {
            Light.range = referenceRadius;
            basicInfo.isActive = isWorking = false;
        }

        previouslyEnabled = Light.enabled;

        if (!isWorking) {
            return;
        }

        Light.range = referenceRadius + lighterRadiusIncrease;
        referenceRadius = Light.range - lighterRadiusIncrease;
    }
}
