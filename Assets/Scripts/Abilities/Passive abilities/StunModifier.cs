﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(fileName = "Stun", menuName = "Passive ability/Modifiers/Stun modifier")]
public class StunModifier : PassiveAbility
{
    public float stunTime;
    public float visibilityDecreaseTime;
    public float visibilityDecreaseAmount;

    private float currentTime;
    private Thinker thinker;

    public override void Apply(GameObject target)
    {
        Debug.Log(currentTime);
        if (currentTime < stunTime) {
            Debug.Log("Thinker disabled");
            thinker.enabled = false;
        } else if (currentTime < stunTime + visibilityDecreaseTime) {
            thinker.enabled = true;
            //TODO Decrease visibility
        } else {
            basicInfo.isActive = false;
        }

        currentTime += Time.deltaTime;
    }

    public override void Init(GameObject target)
    {
        Debug.Log("Init");
        thinker = target.GetComponent<Thinker>();
        currentTime = 0f;
        basicInfo.isActive = true;
    }
}
