﻿using System;
using UnityEngine;
using System.Collections;

public enum AbilityTargetType
{
    Сharacter, GroundSpot
}

public class ActiveAbility : Ability
{
    public ActiveAbilityInfo activeAbilityInfo;

    public virtual void Activate() {}
    public virtual void Activate(float channelingTime) {}
    public virtual void Activate(RaycastHit targetSpot) {}
    public virtual void Activate(RaycastHit targetSpot, float channelingTime) {}
}

[Serializable]
public class ActiveAbilityInfo
{
    public bool requiresTarget;
    public LayerMask targetLayers;
    public bool channeling;
    public bool impactWhileChanneling;
    public float maxChannelingTime;
}
