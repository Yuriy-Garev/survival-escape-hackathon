﻿using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float speed = 6f;            // The speed that the player will move at.
    public float sprintSpeed = 12f;
    public float sneakySpeed = 3f;
    public KeyCode sprintKey = KeyCode.LeftShift;
    public KeyCode sneakKey = KeyCode.LeftControl;
    public float sprintCooldown;
    public float sprintDuration;

    private Transform cameraTransform;
    private Vector3 movement;                   // The vector to store the direction of the player's movement.
    private Animator anim;                      // Reference to the animator component.
    private Rigidbody playerRigidbody;          // Reference to the player's rigidbody.
    private int floorMask;                      // A layer mask so that a ray can be cast just at gameobjects on the floor layer.
    private float camRayLength = 100f;          // The length of the ray from the camera into the scene.
    private float h, v;

    private bool isSneaky;
    private bool isSprinting;
    private float currentSprintCooldown;
    private float sprintStarted;

	public enum MovementPace { Idling, Walking, Running, Sneaking }

	public MovementPace Pace 
	{
		get
		{
			if (isSprinting) {
				return MovementPace.Running;
			}

			if (isSneaky) {
				return MovementPace.Sneaking;
			}

			if (h > 0f || v > 0f) {
				return MovementPace.Walking;
			}

			return MovementPace.Idling;
		}
	}

    private float CurrentSpeed
    {
        get
        {
            if (isSprinting) {
                return sprintSpeed;
            }

            if (isSneaky) {
                return sneakySpeed;
            }

            return speed;
        }
    }

    void Awake()
    {
        // Create a layer mask for the floor layer.
        floorMask = LayerMask.GetMask("Floor");

        // Set up references.
        anim = GetComponent<Animator>();
        playerRigidbody = GetComponent<Rigidbody>();
        cameraTransform = Camera.main.transform;
    }

    private void Update ()
    {
        GetInput();

        currentSprintCooldown -= Time.deltaTime;
        if (isSprinting && Time.time - sprintStarted > sprintDuration) {
            isSprinting = false;
        }
    }


    void FixedUpdate()
    {
        // Move the player around the scene.
        Move();

        // Turn the player to face the mouse cursor.
        Turning();

        // Animate the player.
        Animating();
    }

    private void GetInput ()
    {
        // Store the input axes.
        h = Input.GetAxisRaw("Horizontal");
        v = Input.GetAxisRaw("Vertical");

        if (Input.GetKeyDown(sprintKey) && currentSprintCooldown <= 0f) {
            isSneaky = false;
            isSprinting = true;

            sprintStarted = Time.time;
            currentSprintCooldown = sprintCooldown + sprintDuration;
        }

        if (Input.GetKeyDown(sneakKey)) {
            isSprinting = false;
            isSneaky = !isSneaky;
        }
    }

    void Move()
    {
        movement = cameraTransform.forward * v + cameraTransform.right * h;
        movement = Vector3.ProjectOnPlane(movement, Vector3.up);
        movement = movement.normalized * CurrentSpeed * Time.deltaTime;

        playerRigidbody.MovePosition(transform.position + movement);
    }

    void Turning()
    {
        // Create a ray from the mouse cursor on screen in the direction of the camera.
        Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);

        // Create a RaycastHit variable to store information about what was hit by the ray.
        RaycastHit floorHit;

        // Perform the raycast and if it hits something on the floor layer...
        if (Physics.Raycast(camRay, out floorHit, camRayLength, floorMask))
        {
            // Create a vector from the player to the point on the floor the raycast from the mouse hit.
            Vector3 playerToMouse = floorHit.point - transform.position;

            // Ensure the vector is entirely along the floor plane.
            playerToMouse.y = 0f;

            // Create a quaternion (rotation) based on looking down the vector from the player to the mouse.
            Quaternion newRotation = Quaternion.LookRotation(playerToMouse);

            // Set the player's rotation to this new rotation.
            playerRigidbody.MoveRotation(newRotation);
        }
    }

    void Animating()
    {
        // Create a boolean that is true if either of the input axes is non-zero.
        bool walking = h != 0f || v != 0f;

        // Tell the animator whether or not the player is walking.
        anim.SetBool("IsWalking", walking);
    }
}