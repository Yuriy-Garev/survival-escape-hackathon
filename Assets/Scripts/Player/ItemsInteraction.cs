﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.EventSystems;

public class ItemsInteraction : MonoBehaviour
{
    public Color highlightColor = Color.yellow;
    public float highlightIntencity = 0.2f;
    public ObjectMenu menu;

    private int layerMask;
    private Camera mainCamera;
    private HashSet<Renderer> currentObjectRenderer;
    private EventSystem eventSystem;

    private void Awake ()
    {
        layerMask = LayerMask.GetMask("Interactable");
        eventSystem = EventSystem.current;
        mainCamera = Camera.main;

        currentObjectRenderer = new HashSet<Renderer>();
    }

    private void Update ()
    {
        GetObject();
        HandleMouseClick();
    }

    private void GetObject ()
    {
        RaycastHit hit;
        var ray = mainCamera.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit, Mathf.Infinity, layerMask)) {
            var targetObject = new HashSet<Renderer>(hit.collider.GetComponentsInChildren<Renderer>());
            if (!targetObject.IsProperSubsetOf(currentObjectRenderer)) {
                SetColor(0f);
            }

            currentObjectRenderer = targetObject;
            SetColor(0.2f);
        } else {
            SetColor(0f);
            currentObjectRenderer.Clear();
        }
    }

    private void HandleMouseClick ()
    {
        if (!Input.GetMouseButtonDown(0) || currentObjectRenderer == null || eventSystem.IsPointerOverGameObject()) {
            return;
        }

        foreach (var rend in currentObjectRenderer) {
            menu.Show(GetFuntionalObject(rend));
            break;
        }
    }

    private GameObject GetFuntionalObject (Renderer rend)
    {
        return rend.gameObject.GetComponent<ObjectProperty>() != null
            ? rend.gameObject
            : rend.transform.parent.gameObject;
    }

    private void SetColor (float value)
    {
        if (currentObjectRenderer == null) {
            return;
        }

        value = Mathf.LinearToGammaSpace(value);

        foreach (var mat in currentObjectRenderer
            .Where(rend => rend != null)
            .Select(rend => rend.material)) {
            mat.EnableKeyword("_EMISSION");
            mat.globalIlluminationFlags = MaterialGlobalIlluminationFlags.RealtimeEmissive;
            mat.SetColor("_EmissionColor", highlightColor * value);
        }
    }
}
