﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;

public class Lighter : MonoBehaviour
{
    public float safeZoneTime;
    public float safeZoneCooldown;
    public float redZoneTime;
    public float redZoneCooldown;
    public float emergencyShutdownCooldown;

    public float reactionThresholdTime = 0.1f;

    public KeyCode button = KeyCode.LeftAlt;
    public Light lighter;
    public GameObject uiElement;
    public Image uiImage;

    private float timeStart;
    private float cooldownLeft;
    private bool isReady = true;

    private float TimeAvalaible
    {
        get { return safeZoneTime + redZoneTime; }
    }

    private float Cooldown
    {
        get
        {
            var period = Time.time - timeStart;

            if (period <= safeZoneTime + reactionThresholdTime) {
                return safeZoneCooldown;
            }

            if (period <= safeZoneTime + redZoneTime) {
                return redZoneCooldown;
            }

            return emergencyShutdownCooldown;
        }
    }

    private void Update ()
    {
        if (lighter.enabled) {
            var active = Time.time - timeStart <= TimeAvalaible;
            SetActive(active);


            uiImage.fillAmount = (Time.time - timeStart) / TimeAvalaible;
            uiImage.color = Time.time - timeStart < safeZoneTime
                ? Color.green
                : Color.red;

            cooldownLeft = Cooldown;
            isReady = active;
        } else {
            cooldownLeft -= Time.deltaTime;
            isReady = cooldownLeft <= 0f;
        }

        if (!isReady) {
            return;
        }

        if (Input.GetKeyDown(button)) {
            timeStart = Time.time;
            SetActive(true);
        } else if (Input.GetKeyUp(button)) {
            SetActive(false);
            cooldownLeft = Cooldown;
        }
    }

    private void SetActive (bool active)
    {
        lighter.enabled = active;
        uiElement.SetActive(active);
    }
}
