﻿using UnityEngine;
using System.Collections;

public class PlayerNoise : MonoBehaviour
{
	public float walkingNoise;
	public float runningNoise;
	public float sneakingNoise;

	private PlayerMovement movement;

	public static event Distraction.NoiseDelegate OnMovementNoise;

	private void Awake()
	{
		movement = GetComponent<PlayerMovement> ();
	}

	private void Update()
	{
		var noise = GetNoise ();

		if (OnMovementNoise != null) {
			OnMovementNoise (noise);
		}
	}

	private NoiseInfo GetNoise()
	{
		float noiseAmount = 0f;

		switch (movement.Pace) {
			case PlayerMovement.MovementPace.Idling:
				noiseAmount = 0f;
				break;
			case PlayerMovement.MovementPace.Walking:
				noiseAmount = walkingNoise;
				break;
			case PlayerMovement.MovementPace.Running:
				noiseAmount = runningNoise;
				break;
			case PlayerMovement.MovementPace.Sneaking:
				noiseAmount = sneakingNoise;
				break;
		}

	    return new NoiseInfo {
	        amount = noiseAmount,
	        sourcePosition = transform.position
	    };
	}
}
