﻿using UnityEngine;
using System.Collections.Generic;

public class Equipment : MonoBehaviour
{
    public Transform handEquipmentSpot;

    private Dictionary<EquipmentSlot, InventoryItem> equipment;
    private GameObject handSlotItem;
     
    private static Equipment instance;

    public static void Equip (InventoryItem item)
    {
        var equipped = item.Equipable && !item.IsEquiped;

        if (!equipped) {
            return;
        }

        if (item.EquipmentSlot == EquipmentSlot.Hand) {
            var handItem = Inventory.InstantiateItem(item);
            equipped &= handItem != null;

            if (!equipped) {
                return;
            }

            handItem.transform.position = instance.handEquipmentSpot.position;
            handItem.transform.rotation = instance.handEquipmentSpot.rotation;
            handItem.transform.SetParent(instance.handEquipmentSpot.parent);

            var rigidbody = handItem.GetComponent<Rigidbody>();
            if (rigidbody != null) {
                rigidbody.useGravity = false;
                rigidbody.isKinematic = true;
            }

            foreach (var collider in handItem.GetComponentsInChildren<Collider>()) {
                collider.enabled = false;
            }

            instance.handSlotItem = handItem;
        }

        instance.equipment[item.EquipmentSlot] = item;
        item.IsEquiped = true;

        Inventory.UpdateWindow();
    }

    public static void Unequip (InventoryItem item)
    {
        if (!item.Equipable || !item.IsEquiped) {
            return;
        }

        if (item.EquipmentSlot == EquipmentSlot.Hand) {
            Destroy(instance.handSlotItem);
        }

        instance.equipment[item.EquipmentSlot] = null;
        item.IsEquiped = false;

        Inventory.UpdateWindow();
    }

    private void Awake ()
    {
        InitSingleton();
        InitDictionary();
    }

    private void InitDictionary ()
    {
        equipment = new Dictionary<EquipmentSlot, InventoryItem> {
            {EquipmentSlot.Body, null},
            {EquipmentSlot.Hand, null}
        };
    }

    private void InitSingleton()
    {
        if (instance != null) {
            Destroy(this);
            return;
        }

        instance = this;
    }
}
