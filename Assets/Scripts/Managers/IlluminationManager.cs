﻿using UnityEngine;
using System.Collections;

public class IlluminationManager : MonoBehaviour
{
    public Light globalIllumination;
    public AnimationCurve illuminationCurve;

    public float period;

    private float currentTime;

    private void Update ()
    {
        currentTime += Time.deltaTime;
        currentTime = currentTime < period ? currentTime : period - currentTime;

        globalIllumination.intensity = illuminationCurve.Evaluate(currentTime / period);
    }
}
