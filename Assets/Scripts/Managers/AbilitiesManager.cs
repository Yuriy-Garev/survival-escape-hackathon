﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class AbilitiesManager : MonoBehaviour
{
    public static ActiveAbility CurrentAbility
    {
        get { return instance.currentAbility; }
        set { instance.currentAbility = value; }
    }

    public int maxAvailableAbilities;
    public AbilitiesMenu menu;

    private List<Ability> allAbilities;
    private List<Ability> currentlyAvailableAbilities;

    private ActiveAbility currentAbility;
    private Camera mainCamera;

    private bool isChanneling;
    private bool isChoosingTarget;
    private RaycastHit targetHit;
    private float currentChannelingTime;

    private float currentAmnesiaTime;

    private const string Path = "Abilities";

    private static AbilitiesManager instance;

    public static void RunCoroutine (IEnumerator coroutine)
    {
        instance.StartCoroutine(coroutine);
    }

    public static List<T> GetAvailableAbilities<T> () where T : Ability
    {
		var result = instance.currentlyAvailableAbilities.Where (ability => ability is T);
		return result.Cast<T>().ToList();
    }

    public void UseAbility ()
    {
        if (!currentAbility) {
            return;
        }

        if (currentAbility.activeAbilityInfo.requiresTarget) {
            isChoosingTarget = true;
        } else if (currentAbility.activeAbilityInfo.channeling) {
            isChanneling = true;
        } else {
            currentAbility.Activate();
        }
    }

    private void Update ()
    {
        if (Input.GetKeyDown(KeyCode.Escape)) {
            isChoosingTarget = isChanneling = false;
            currentChannelingTime = 0f;
        } else if (Input.GetKeyDown(KeyCode.C)) {
            UseAbility();
        }

        UpdateAbilityStatus();
        UpdateAmnesia();
    }

    private void UpdateAmnesia ()
    {

    }

    private void UpdateAbilityStatus ()
    {
        if (!isChoosingTarget && !isChanneling) {
            return;
        }

        if (isChoosingTarget) {
            if (!Input.GetMouseButtonDown(0)) {
                return;
            }

            RaycastHit hit;
            Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, Mathf.Infinity, currentAbility.activeAbilityInfo.targetLayers)) {
                if (currentAbility.activeAbilityInfo.channeling) {
                    isChanneling = true;
                    targetHit = hit;
                } else {
                    currentAbility.Activate(hit);
                    isChoosingTarget = false;
                }
            }
        } else {
            currentChannelingTime += Time.deltaTime;

            if (currentChannelingTime > currentAbility.activeAbilityInfo.maxChannelingTime) {
                isChanneling = false;
                if (currentAbility.activeAbilityInfo.impactWhileChanneling) {
                    return;
                }

                if (currentAbility.activeAbilityInfo.requiresTarget) {
                    currentAbility.Activate(targetHit, currentChannelingTime);
                } else {
                    currentAbility.Activate(currentChannelingTime);
                }

                return;
            }

            if (!currentAbility.activeAbilityInfo.impactWhileChanneling) {
                return;
            }

            if (currentAbility.activeAbilityInfo.requiresTarget) {
                currentAbility.Activate(targetHit, currentChannelingTime);
            } else {
                currentAbility.Activate(currentChannelingTime);
            }
        }
    }

    private void RefreshAbilities ()
    {
        currentlyAvailableAbilities.Clear();
        var abilities = allAbilities.ToList();

        for (int i = 0; i < maxAvailableAbilities; i++) {
            if (abilities.Count == 0) {
                break;
            }

            var index = UnityEngine.Random.Range(0, abilities.Count);
            currentlyAvailableAbilities.Add(abilities[index]);
            abilities.RemoveAt(index);
        }
    }

    private void RandomizeCurrentAbility ()
    {
        var activeAbilities = GetAvailableAbilities<ActiveAbility>();
        currentAbility = activeAbilities[UnityEngine.Random.Range(0, activeAbilities.Count)];
    }

    private void Awake ()
    {
        if (instance) {
            Destroy(this);
            return;
        }

        instance = this;
        mainCamera = Camera.main;
        currentlyAvailableAbilities = new List<Ability>();
    }

    private void Start ()
    {
        LoadAbilities();
        Amensia();
    }

    private void Amensia ()
    {
        RefreshAbilities();
        RandomizeCurrentAbility();
        menu.UpdateMenu();
    }

    private void LoadAbilities ()
    {
        allAbilities = Resources.LoadAll<Ability>(Path).ToList();
    }
}
