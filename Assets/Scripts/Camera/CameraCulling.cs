﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

[RequireComponent(typeof(Camera))]
public class CameraCulling : MonoBehaviour
{
    [Range(0f, 1f)]
    public float targetAlfa;
    public LayerMask cullingMask;
    public Transform player;

    private HashSet<Renderer> previousRenderers;
     
    private float DistanceToPlayer
    {
        get { return Vector3.Distance(transform.position, player.position); }
    }

    private void Awake ()
    {
        previousRenderers = new HashSet<Renderer>();
    }
	
	void Update ()
	{
	    var hits = Physics.RaycastAll(transform.position, transform.forward, DistanceToPlayer, cullingMask);
	    var renderers = GetRenderers(hits).ToArray();
	    SetTransparency(renderers, targetAlfa);
	    previousRenderers.ExceptWith(renderers);
	    SetTransparency(previousRenderers, 1f);
        previousRenderers = new HashSet<Renderer>(renderers);
	}

    private void SetTransparency (IEnumerable<Renderer> renderers, float transparency)
    {
        foreach (var material in renderers.Select(rend => rend.material)) {
            var color = material.color;
            color.a = transparency;
            material.color = color;
        }
    }

    private IEnumerable<Renderer> GetRenderers (RaycastHit[] hits)
    {
        return hits
            .SelectMany(hit => hit.collider.GetComponentsInChildren<Renderer>())
            .Where(rend => rend != null);
    }
}
