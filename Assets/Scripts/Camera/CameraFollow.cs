﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour
{
    public Transform target;            // The position that that camera will be following.
    public float smoothing = 5f;        // The speed with which the camera will be following.
    public float angularSpeed = 30f;

    private Vector3 offset;                     // The initial offset from the target.
    private float rotateAxis;

    void Start()
    {
        // Calculate the initial offset.
        offset = transform.position - target.position;
    }

    private void Update ()
    {
        rotateAxis = Input.GetAxis("Camera horizontal");
    }

    void FixedUpdate()
    {
        offset = Quaternion.AngleAxis(rotateAxis * angularSpeed * Time.deltaTime, target.up) * offset;
        Vector3 targetCamPos = target.position + offset;

        transform.position = Vector3.Lerp(transform.position, targetCamPos, smoothing * Time.deltaTime);
        transform.LookAt(target);
    }
}